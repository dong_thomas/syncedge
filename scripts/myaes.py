from Crypto.Cipher import AES
from Crypto import Random
import base64

BS = AES.block_size
pad = lambda s: s + (BS - len(s) % BS) * chr(BS - len(s) % BS)
unpad = lambda s : s[0:-ord(chr(s[-1]))]

class AES_HELPER(object):

    def __init__(self):
        print("init")

    def key_generator(self, bits=256):
        random = Random.new()
        blen = int(bits/8)
        print(blen)
        key = random.read(blen)
        return base64.b64encode(key)

    def iv_generator(self):
        '''
        AES Initialization Vector is 128 bits(16 bytes).
        '''
        random = Random.new()
        key = random.read(16)
        return base64.b64encode(key)

    def encrypt(self, b64key, b64iv, data):
        key = base64.b64decode(b64key)
        iv = base64.b64decode(b64iv)
        cryptor = AES.new(key, AES.MODE_CBC, iv)
        data = pad(data)
        encrypted = cryptor.encrypt(data)
        encrypted = base64.b64encode(encrypted)
        return encrypted

    def decrypt(self, b64key, b64iv, data):
        key = base64.b64decode(b64key)
        iv = base64.b64decode(b64iv)
        cryptor = AES.new(key, AES.MODE_CBC, iv)
        decrypted = base64.b64decode(data)
        print(decrypted)
        decrypted = cryptor.decrypt(decrypted)
        print(decrypted)
        decrypted = unpad( decrypted )
        return decrypted

aes_helper = AES_HELPER()

if __name__ == '__main__':
    key = aes_helper.key_generator()
    iv = aes_helper.iv_generator()
    
    key = "xxxx".ljust(32, "0")
    iv = "yyyy".ljust(16, "0")
    key = base64.b64encode(bytes(key, 'utf-8'))
    iv = base64.b64encode(bytes(iv, 'utf-8'))
    data = "Hello moto. I'm fire. Thank you. I got fired. Thank you. Madadabi meow meow Madadabi."
    encrypted = aes_helper.encrypt(key, iv, data)
    print(type(encrypted))
    decrypted = aes_helper.decrypt(key, iv, encrypted)
    print ('data: %s' % data )
    print ('encrypted: %s' % encrypted )
    print ('decrypted: %s' % decrypted )

    with open("encf", 'wb') as out:
        out.write(encrypted)

    with open("encf") as fin:
        enc_data=fin.read()
        ndecrypted = aes_helper.decrypt(key, iv, enc_data)
        print ('ndecrypted: %s' % ndecrypted )
