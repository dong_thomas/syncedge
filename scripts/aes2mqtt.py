# -*- coding: utf-8 -*-
import time
import paho.mqtt.client as mqtt
import myaes
import base64

# If broker asks client ID.
client_id = ""

client = mqtt.Client(client_id=client_id)

# If broker asks user/password.
user = "thomas"
password = "okok7480"
client.username_pw_set(user, password)

client.connect("localhost")

topic = "Sensor/rocksaying"
payload = "mqtt"

aes_helper = myaes.AES_HELPER()
key = "xxxx".ljust(32, "0")
iv = "yyyy".ljust(16, "0")
key = base64.b64encode(bytes(key, 'utf-8'))
iv = base64.b64encode(bytes(iv, 'utf-8'))
for i in range(500000):
    payloadx = "{0}-{1}".format(payload, i)
    print(payloadx)
    cipherpayload = aes_helper.encrypt(key, iv, payloadx)
    print(cipherpayload)
    client.publish(topic, cipherpayload)
    print(aes_helper.decrypt(key, iv, cipherpayload))
    time.sleep(1)
