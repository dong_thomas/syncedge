# coding: utf-8
import sys, os, time, signal
import paho.mqtt.client as mqtt
import re
import json
import datetime
import myaes
import base64
mqtt_looping = False

TOPIC_ROOT = "Sensor"

aes_helper = myaes.AES_HELPER()

def on_connect(mq, userdata, rc, _):
    # subscribe when connected.
    mq.subscribe(TOPIC_ROOT + '/#')

def on_message(mq, userdata, msg):
    global aes_helper
    #print( "topic: %s" % msg.topic)
    #print( "payload: %s" % msg.payload)
    #print( "qos: %d" % msg.qos)
    strSensorData = msg.payload
    print(strSensorData)
    key = "xxxx".ljust(32, "0")
    iv = "yyyy".ljust(16, "0")
    key = base64.b64encode(bytes(key, 'utf-8'))
    iv = base64.b64encode(bytes(iv, 'utf-8'))
    decryptedup = aes_helper.decrypt(key, iv, strSensorData)
    print(decryptedup)

def mqtt_client_thread():
    global client, mqtt_looping
    client_id = "" # If broker asks client ID.
    client = mqtt.Client(client_id=client_id)

    # If broker asks user/password.
    user = "thomas"
    password = "okok7480"
    client.username_pw_set(user, password)

    client.on_connect = on_connect
    client.on_message = on_message

    try:
        client.connect("127.0.0.1")
    except:
        print ("MQTT Broker is not online. Connect later.")

    mqtt_looping = True
    print ("Looping...")

    #mqtt_loop.loop_forever()
    cnt = 0
    while mqtt_looping:
        client.loop()

        cnt += 1
        if cnt > 20:
            try:
                client.reconnect() # to avoid 'Broken pipe' error.
            except:
                time.sleep(1)
            cnt = 0

    # 關閉queue連線
    connection.close()
    # queue

    print ("quit mqtt thread")
    client.disconnect()

def stop_all(*args):
    global mqtt_looping
    mqtt_looping = False

if __name__ == '__main__':
    signal.signal(signal.SIGTERM, stop_all)
    signal.signal(signal.SIGQUIT, stop_all)
    signal.signal(signal.SIGINT,  stop_all)  # Ctrl-C

    mqtt_client_thread()

    print ("exit program")
    sys.exit(0)
