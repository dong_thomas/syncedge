# coding: utf-8
import sys, os, time, signal
import paho.mqtt.client as mqtt
import re
import json
import datetime

mqtt_looping = False

TOPIC_ROOT = "LASS/Test"

def on_connect(mq, userdata, rc, _):
    # subscribe when connected.
    mq.subscribe(TOPIC_ROOT + '/#')

def on_message(mq, userdata, msg):
    #print( "topic: %s" % msg.topic)
    #print( "payload: %s" % msg.payload)
    #print( "qos: %d" % msg.qos)
    #if 'Test' in msg.topic:
    #    return
    strSensorData = str(msg.payload)
    strSensorData.replace("'", "")
    #print(strSensorData)
    items = re.split('\|', strSensorData)
    flag = 0
    sensorData = {}
    sensorData['topic'] = msg.topic
    #global value_dust
    #global value_pm10
    #global value_humidity
    #global value_temperature
    #global str_date
    #global str_time
    for item in items:
        if item == '':
            continue 
        item.replace("'", "")
        pairs = re.split('=',item)

        if (len(items)==1):
            continue
        flag = 1
        #if (pairs[0] == "device_id"):
        #    value_device_id = pairs[1]
        #elif (pairs[0] == "s_d0"):
        #    value_dust = pairs[1]
        #elif (pairs[0] == "s_t0"):
        #    value_temperature = pairs[1]
        #elif (pairs[0] == "s_h0"):
        #    value_humidity = pairs[1]
        #elif (pairs[0] == "s_d1"):
        #    value_pm10 = pairs[1]
        #elif (pairs[0] == "date"):
        #    str_date = pairs[1]
        #elif (pairs[0] == "time"):
        #    str_time = pairs[1]
        if len(pairs) == 2:
            key = ''
            value = ''
            if pairs[0] != '':
                key = pairs[0]
            if pairs[1] != '':
                value = pairs[1]
            sensorData[key] = value
    if (flag==0):
        return

    #lassdata = "{0} {1} {2} {3} from {4}".format(str_time, str_date, value_dust, value_temperature, msg.topic)

    #print(sensorData)
    print(json.dumps(sensorData))
    hourFile = time.strftime("%Y-%m-%d-%H", time.localtime())
    with open(hourFile, "a+") as inFile:
        inFile.write(json.dumps(sensorData))

def mqtt_client_thread():
    global client, mqtt_looping
    client_id = "" # If broker asks client ID.
    client = mqtt.Client(client_id=client_id)

    # If broker asks user/password.
    user = ""
    password = ""
    client.username_pw_set(user, password)

    client.on_connect = on_connect
    client.on_message = on_message

    try:
        client.connect("gpssensor.ddns.net")
    except:
        print ("MQTT Broker is not online. Connect later.")

    mqtt_looping = True
    print ("Looping...")

    #mqtt_loop.loop_forever()
    cnt = 0
    while mqtt_looping:
        client.loop()

        cnt += 1
        if cnt > 20:
            try:
                client.reconnect() # to avoid 'Broken pipe' error.
            except:
                time.sleep(1)
            cnt = 0

    # 關閉queue連線
    connection.close()
    # queue

    print ("quit mqtt thread")
    client.disconnect()

def stop_all(*args):
    global mqtt_looping
    mqtt_looping = False

if __name__ == '__main__':
    signal.signal(signal.SIGTERM, stop_all)
    signal.signal(signal.SIGQUIT, stop_all)
    signal.signal(signal.SIGINT,  stop_all)  # Ctrl-C

    mqtt_client_thread()

    print ("exit program")
    sys.exit(0)
