from django.contrib import admin
from .models import DeviceType, Device, DeviceToken

# Register your models here.

@admin.register(DeviceType)
class DeviceTypeAdmin(admin.ModelAdmin):
    list_display = ('id', 'type_name')

@admin.register(Device)
class DeviceAdmin(admin.ModelAdmin):
    list_display = ('title','device_type','author','aeskey','ivkey','created_time','last_updated_time')

@admin.register(DeviceToken)
class DeviceTokenAdmin(admin.ModelAdmin):
    list_display = ('title','deviceID','author','token','s3key','s3seckey','created_time','last_updated_time')

