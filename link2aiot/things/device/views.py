from django.shortcuts import render

# Create your views here.
from device.models import Device
from device.models import DeviceToken
from device.serializers import DeviceSerializer
from device.serializers import DeviceTokenSerializer

from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated
from rest_framework import generics

# Create your views here.
class DeviceViewSet(viewsets.ModelViewSet):
    queryset = Device.objects.all()
    serializer_class = DeviceSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        user = self.request.user
        return Device.objects.filter(author=user)

class DeviceTokenViewSet(viewsets.ModelViewSet):
    queryset = DeviceToken.objects.all()
    serializer_class = DeviceTokenSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        user = self.request.user
        return DeviceToken.objects.filter(author=user)

