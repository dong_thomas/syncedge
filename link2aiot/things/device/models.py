from django.db import models
from django.contrib.auth.models import User
# Create your models here.

class DeviceType(models.Model):
    type_name = models.CharField(max_length=15)

    def __str__(self):
        return self.type_name

class Device(models.Model):
    title = models.CharField(max_length=50)
    deviceID = models.CharField(max_length=50)
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    aeskey = models.CharField(max_length=256)
    ivkey = models.CharField(max_length=32)
    created_time = models.DateTimeField(auto_now_add = True)
    last_updated_time = models.DateTimeField(auto_now = True)
    device_type = models.ForeignKey(DeviceType, on_delete=models.CASCADE)

    def __str__(self):
        return "<Device: %s>" % self.title

class DeviceToken(models.Model):
    title = models.CharField(max_length=50)
    deviceID = models.ForeignKey(Device, on_delete=models.CASCADE)
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    token = models.CharField(max_length=256)
    s3key = models.CharField(max_length=256)
    s3seckey = models.CharField(max_length=256)
    created_time = models.DateTimeField(auto_now_add = True)
    last_updated_time = models.DateTimeField(auto_now = True)

    def __str__(self):
        return "<Token: %s>" % self.title
