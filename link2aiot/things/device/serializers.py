from rest_framework import serializers
from device.models import Device 
from device.models import DeviceToken

class DeviceTokenSerializer(serializers.ModelSerializer):
    class Meta:
        model = DeviceToken
        fields = '__all__'
        #fields = ('id', 'song', 'singer', 'last_modify_date', 'created')

class DeviceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Device
        fields = '__all__'
        #fields = ('id', 'song', 'singer', 'last_modify_date', 'created')
